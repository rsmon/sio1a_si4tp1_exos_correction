
package programmes;

import java.util.Random;
import java.util.Scanner;

public class Programme0f {
 
    public static void main(String[] args) {
        
        Scanner  clavier     = new Scanner(System.in);
        Random   aleat       = new Random();
        
        int      nbADeviner  = aleat.nextInt(1000)+1;
        int      nbPropose   = 0;
        
        boolean  trouve=false;
        int      nbEssais=0;
        
        System.out.println("Quel nombre proposez-vous?");
        nbPropose=clavier.nextInt();
        
        while (trouve==false)
        {
            
          nbEssais++;   
        
          if(nbADeviner==nbPropose){
             trouve=true;
          }
          else
          {    
             if(nbPropose<nbADeviner)
             {    
                System.out.println("Le nombre à deviner est plus grand que "+nbPropose);
             }
             else
             {
                System.out.println("Le nombre à deviner est plus petit que "+nbPropose);
             }
             
             System.out.println("Quel nombre proposez-vous?");
             nbPropose=clavier.nextInt();
          }
        }
        
        System.out.println("Vous avez trouvé en "+ nbEssais+" essais");
    }
}
