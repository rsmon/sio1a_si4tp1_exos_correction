package programmes;

import java.util.Random;
import java.util.Scanner;

public class Programme0b {

    public static void main(String[] args) {
       
        Scanner  scan=new Scanner(System.in);
        String   continuer="O";
        
        int nbADeviner;
        int nbPropose;
      
        Random rd=new Random();
                
        while ( continuer.equals("O")){
        
          nbADeviner=rd.nextInt(10)+1;
        
          System.out.println("Proposez un nombre entre 1 et 10");
          nbPropose=scan.nextInt();
        
          if(nbPropose == nbADeviner){
        
            System.out.println("Vous avez gagné!");
          }
          else{
        
            System.out.println("Vous avez perdu!");
            System.out.println("Le nombre à deviner était: "+ nbADeviner);
          }
      
          System.out.println();
          
          System.out.println("Voulez vous continuer? (répondre  O ou N)");
          continuer=scan.next();
        }
    }
}

