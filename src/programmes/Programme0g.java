package programmes;


import java.util.Scanner;


public class Programme0g {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);

        float chiffreAffaires=0;
        float commission=0;
        
        System.out.println("Indiquez en Euros, le chiffre d'affaires que avez-vous réalisé?");
        chiffreAffaires=clavier.nextFloat();
        
        
        if(chiffreAffaires<10000)
        {
          commission=chiffreAffaires*0.02f;
        }
        else if ( chiffreAffaires<20000)
        {
          commission=200+(chiffreAffaires- 10000)*0.04f;
        } 
        else
        {
          commission=600+(chiffreAffaires- 20000)*0.06f;
           
        }
        
        System.out.println("Votre commission s'élève à "+commission+" €");
        
    }
}
