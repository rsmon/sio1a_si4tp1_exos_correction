
package programmes;

import java.util.Scanner;

public class Programme0e {

    public static void main(String[] args) {

        Scanner clavier= new Scanner(  System.in );
        
        float  noteSaisie, total=0;
        int    nbNotes=0;
        
        float  moyenne;
        
        System.out.println("Entrez une note (-1 pour terminer)");
        noteSaisie=clavier.nextFloat();
        
        while( noteSaisie != -1 ){
        
            total += noteSaisie; // totaliser noteSaisie dans total
            nbNotes++; // Ajouter  1  ( on dit incrementer ) a nbNotes
        
            System.out.println("Entrez une note (-1 pour terminer)");
            noteSaisie=clavier.nextFloat();
        
        }
        
        if( nbNotes>0 ){
           
           moyenne=total/nbNotes;
           System.out.printf("\nLa moyenne vaut %2.2f\n\n", moyenne);
        }
        else{
            
           System.out.println("\nVous n'avez saisi aucune note !\n"); 
        }    
        
    }
}
