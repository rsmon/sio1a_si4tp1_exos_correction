
package programmes;

import java.util.Scanner;

public class Programme0d {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(  System.in );
        
        float largeur, longueur;
        
        float perimetre, surface;
        
        System.out.println("Entrez la largeur");
        largeur=clavier.nextFloat();
        
        System.out.println("Entrez la longueur");
        longueur=clavier.nextFloat();
          
        perimetre = 2*( largeur + longueur );
        surface   = largeur*longueur;
        
//        
//        System.out.println("Le périmétre vaut:  "+perimetre);
//        System.out.println();
//        System.out.println("La surface vaut: "+surface);
        
        
        System.out.printf("Le périmètre vaut: %5.2f\n",perimetre);
        
        System.out.printf("La surface vaut: %5.2f\n\n",surface);
    }
}
