package programmes;

import java.util.Random;
import java.util.Scanner;

public class Programme0a {

    public static void main(String[] args) {
       
        Scanner  clavier = new Scanner(System.in);
        Random   rd   = new Random();
        
        int nbADeviner, nbPropose;
      
        nbADeviner=rd.nextInt(10)+1;
        
        System.out.println("Proposez un nombre entre 1 et 10");
        nbPropose=clavier.nextInt();
        
        if(nbPropose == nbADeviner)
        {
        
            System.out.println("Vous avez gagné!");
        }
        else
        {
            System.out.println("Vous avez perdu!");
            System.out.println("Le nombre à deviner était: "+ nbADeviner);
        }          
    }
}

