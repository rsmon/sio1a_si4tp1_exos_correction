
package programmes;

import java.util.Scanner;

public class Programme0 {

    public static void main(String[] args) {
      
       int x, y, z;
       
       Scanner clavier=new Scanner(System.in);
       
       System.out.println("Entrez le premier nombre entier");
       x=clavier.nextInt();
         
       System.out.println("Entrez le deuxième nombre entier");
       y=clavier.nextInt();
       
       
       z=x+y;
       
       System.out.println("La somme des deux nombres vaut:"+ z);
    }
}